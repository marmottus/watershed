#ifndef MARKER_HH_
# define MARKER_HH_

# include <vector>
# define UNMARKED -1

typedef struct s_matrix_pos
{
  int row;
  int col;
} s_matrix_pos;

typedef struct s_marker
{
  s_matrix_pos*	pos;
  uchar		intensity;
  int		label;
  bool		in_queue;
} s_marker;

typedef std::vector<s_marker*>		t_vector_marker;
typedef std::vector<t_vector_marker>	t_matrix_marker;
typedef std::vector<s_matrix_pos*>	t_vector_matrix_pos;

#endif /* !MARKER_HH_ */
