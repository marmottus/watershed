#ifndef COMPARE_HH_
# define COMPARE_HH_

# include <queue>
# include <marker.hh>

class Compare
{
public:
  inline bool operator()(const s_marker* l1, const s_marker* l2)
  {
    return l1->intensity < l2->intensity;
  }
};

typedef std::priority_queue<s_marker*, t_vector_marker, Compare>
t_pqueue_marker;

#endif /* !COMPARE_HH_ */
