#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

#include <iostream>

#include <compare.hh>
#include <marker.hh>

typedef std::vector<cv::Vec3b*>	t_vector_Vec3b;
static t_vector_matrix_pos	basins;


// Mouse release event
static void my_mouse_callback(int event, int x, int y,
			      int flags, void* data)
{
  if (event == CV_EVENT_LBUTTONDOWN)
  {
    s_matrix_pos*	p = new s_matrix_pos;
    cv::Mat*		img = reinterpret_cast<cv::Mat*>(data);
    p->row = y;
    p->col = x;
    img->at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 0);
    basins.push_back(p);
  }
}

// Convert img into grayscale image
static cv::Mat grayscale(const cv::Mat& img)
{
  cv::Mat gray_img(img.rows, img.cols, img.type());

  for (int r = 0; r < img.rows; ++r)
  {
    for (int c = 0; c < img.cols; ++c)
    {
      cv::Vec3b	v = img.at<cv::Vec3b>(r, c);
      uchar	v_gray = (v[0]+v[1]+v[2])/3;

      gray_img.at<cv::Vec3b>(r, c) = cv::Vec3b(v_gray, v_gray, v_gray);
    }
  }

  return gray_img;
}

// Return neighbors of marker
static t_vector_marker neighborhood(const cv::Mat&		img,
				    const s_marker*		marker,
				    const t_matrix_marker&	markers)
{
  s_matrix_pos*		p = marker->pos;
  t_vector_marker	v;
  int			r = p->row;
  int			c = p->col;
  s_matrix_pos		neighbors[8] =
    {
      { r, c + 1 },	// east
      { r, c - 1} ,	// west
      { r - 1, c },	// north
      { r + 1, c },	// south
      { r + 1, c + 1 },	// south east
      { r + 1, c - 1 },	// south west
      { r - 1, c - 1 },	// north west
      { r - 1, c + 1 },	// north east
    };

  for (int i = 0; i < 8; ++i)
  {
    s_matrix_pos pn = neighbors[i];
    if (pn.row >= 0 && pn.row < img.rows &&
	pn.col >= 0 && pn.col < img.cols)
    {
      v.push_back(markers[pn.row][pn.col]);
    }
  }

  return v;
}

// Free all memory!
static void free_all(const t_vector_Vec3b& colors,
		     const cv::Mat& img,
		     const t_matrix_marker& markers)
{
  for (uint i = 0; i < basins.size(); ++i)
    delete basins[i];
  for (uint i = 0; i < colors.size(); ++i)
    delete colors[i];
  for (int r = 0; r < img.rows; ++r)
  {
    for (int c = 0; c < img.cols; ++c)
    {
      s_marker* marker = markers[r][c];
      delete marker->pos;
      delete marker;
    }
  }
}

// init openCV video
static int	init_video(const cv::Mat* new_img)
{
  int		key;

  cv::namedWindow ("video", CV_WINDOW_AUTOSIZE);
  cv::setMouseCallback("video", my_mouse_callback, (void*)new_img);

  do
  {
    cv::imshow ("video", *new_img);
    key = cv::waitKey (30) % 256;
  } while (key != '\n' && key != 'q');

  if (key == 'q')
    return (-1);

  return (0);
}

// Insert all neighbors of each marked area
static void insert_markers(const cv::Mat& img,
			   const t_matrix_marker& markers,
			   t_pqueue_marker& queue)
{
  for (uint i = 0; i < basins.size(); ++i)
  {
    s_marker*		marker = markers[basins[i]->row][basins[i]->col];
    t_vector_marker	vn;

    if (marker->label != UNMARKED) // already marked
      continue;

    marker->label = i;

    // Put all neighbors of pixel into the queue
    vn = neighborhood(img, marker, markers);
    for (uint j = 0; j < vn.size(); ++j)
    {
      s_marker* nmarker = vn[j];
      if (not marker->in_queue)
      {
	nmarker->in_queue = true;
	queue.push(nmarker);
      }
    }
  }

}

// init all markers
static void	init_markers(const cv::Mat& img, t_matrix_marker& markers)
{
  cv::Mat	gray_img = grayscale(img);

  for (int r = 0; r < img.rows; ++r)
  {
    markers.push_back(t_vector_marker(img.cols));
    for (int c = 0; c < img.cols; ++c)
    {
      s_matrix_pos*	p = new s_matrix_pos;
      s_marker*	marker = new s_marker;

      marker->intensity = gray_img.at<cv::Vec3b>(r, c)[0];
      p->row = r;
      p->col = c;
      marker->pos = p;
      marker->label = UNMARKED;
      marker->in_queue = false;

      markers[r][c] = marker;
    }
  }
}

// init markers colors
static void init_colors(t_vector_Vec3b& colors)
{
  int next_color = 0;
  for (uint i = 0; i < basins.size(); ++i)
  {
    int random = std::rand() % 255;
    int b_color = (i*100)/basins.size();
    if (next_color == 0) // red
      colors.push_back(new cv::Vec3b(0, random, b_color));
    else if (next_color == 1) // green
      colors.push_back(new cv::Vec3b(random, b_color, 0));
    else // blue
      colors.push_back(new cv::Vec3b(b_color, 0, random));

    next_color = (next_color+1) % 3;
  }

}

// watershed algorithm
static void watershed(t_pqueue_marker& queue,
		      const cv::Mat& img,
		      const t_matrix_marker& markers,
		      const t_vector_Vec3b& colors)

{
  cv::Mat		new_img;
  int			key = 0;

  while (not queue.empty() && key != 'q')
  {
    s_marker*		marker;
    t_vector_marker	vn;
    int			new_label = UNMARKED;
    bool		same_label = true;

    // extract the highest priority marker
    marker = queue.top();
    queue.pop();

    vn = neighborhood(img, marker, markers);
    // Check if all labelled markers have the same label
    for (uint j = 0; j < vn.size() && same_label; ++j)
    {
      s_marker*	nmarker = vn[j];

      if (nmarker->label != UNMARKED)
      {
	if (new_label == UNMARKED)
	  new_label = nmarker->label;
	else
	  same_label = nmarker->label == new_label;
      }
    }

    if (same_label && new_label != UNMARKED)
      marker->label = new_label;

    // Put non labelled markers in queue
    for (uint j = 0; j < vn.size(); ++j)
    {
      s_marker*	nmarker = vn[j];

      if (nmarker->label == UNMARKED && not nmarker->in_queue)
      {
	nmarker->in_queue = true;
	queue.push(nmarker);
      }
    }

    new_img = img.clone();
    // color image
    {
      for (int r = 0; r < img.rows; ++r)
      {
	for (int c = 0; c < img.cols; ++c)
	{
	  if (markers[r][c]->label != UNMARKED)
	    new_img.at<cv::Vec3b>(r, c) = *colors[markers[r][c]->label];
	  else
	    new_img.at<cv::Vec3b>(r, c) = cv::Vec3b(0, 0, 0);
	}
      }
    }

    cv::imshow ("video", new_img);
    key = cv::waitKey(1) % 256;
  }
}

int main(int argc, char *argv[])
{
  if (argc > 1)
  {
    t_pqueue_marker	queue;
    t_matrix_marker	markers;
    cv::Mat		img = cv::imread(std::string(argv[1]));
    cv::Mat		new_img = img.clone();
    t_vector_Vec3b	colors;
    int			key;

    if (init_video(&new_img) < 0)
      return (0);

    init_markers(img, markers);

    insert_markers(img, markers, queue);
    init_colors(colors);

    watershed(queue, img, markers, colors);

    // wait for `q' to exit
    do
    {
      key = cv::waitKey (30) % 256;
    } while (key != 'q');

    free_all(colors, img, markers);
  }

  return (0);
}
