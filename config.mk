### project ###################################################################

AUTHORS                 ?= arthur-crepin-leblond
ARCHIVE_NAME            ?= $(AUTHORS)-$(BIN)

# bzip or gzip
ARCHIVE_TYPE            ?= bzip

# CC or CXX
LANGUAGE                ?= CXX

# name of the binary
BIN                     ?= watershed
# installation prefix

# directories
SRC_DIR                 ?= src
HEADERS_DIR             ?= include
OBJS_DIR                ?= .objs
DEP_FILE                ?= .Makefile.dep

CLEAN_FILES             ?= infos.log errors.log

SOURCES                 ?= src/watershed.cc

# file included in archive
DIST_FILES              ?= src                                          \
                           Makefile                                     \
                           include                                      \
                           config.mk                                    \
                           image.png

# DEBUG macro will be set to 1
DEBUG                   ?= 0


# Arguments passed to the binary when invoking make run
ARGV                    ?=

#### flags ####################################################################

# libraries that can be found with pkg-config
PKG_LIBS                ?= opencv

####  preprocessor flags

# all platforms
FLAGS                   ?= -Iinclude                                    \
                           -Wall -Werror -pedantic

# Linux
LINUX_FLAGS             ?=
# Darwin
DARWIN_FLAGS            ?=

#### linker flags

# all platforms
LINK_FLAGS              ?= -L. -Lsrc
# Linux
LINUX_LINK_FLAGS        ?=
# Darwin
DARWIN_LINK_LAGS        ?=

# binaries ####################################################################
CC                      = gcc
CXX                     ?= g++
LD                      ?= ld
TAR                     ?= tar
ARCH                    ?= $(shell uname -r)
LOG                     ?= "git log"
CHECKSUM                ?= sha1sum
PKG_CONFIG              ?= pkg-config

# install ####################################################################

# installation flags
INSTALL_FLAGS           ?=

#EOF
