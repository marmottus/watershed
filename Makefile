all: $(BIN)

include config.mk

_OBJS			?= $(SOURCES:$(SRC_DIR)/%.c=$(OBJS_DIR)/%.c.o)
_OBJS                   := $(_OBJS:$(SRC_DIR)/%.cc=$(OBJS_DIR)/%.cc.o)
_OBJS                   := $(_OBJS:$(SRC_DIR)/%.cpp=$(OBJS_DIR)/%.cpp.o)

ifeq ($(shell uname), Linux)
FLAGS			+= $(LINUX_FLAGS) $(shell			\
			     for lib in $(PKG_LIBS); do			\
			       $(PKG_CONFIG) --cflags $$lib;		\
		             done)

LINK_FLAGS		+= $(LINUX_LINK_FLAGS) $(shell			\
			     for lib in $(PKG_LIBS); do			\
			       $(PKG_CONFIG) --libs $$lib;		\
		             done)
endif

ifeq ($(shell uname), Darwin)
FLAGS                   += $(DARWIN_FLAGS)
LINK_FLAGS              += $(DARWIN_LINK_FLAGS)
endif

ifeq ($(DEBUG), 1)
FLAGS                   += -DDEBUG=1 -g -ggdb
endif

ifeq ($(ARCHIVE_TYPE), gzip)
  _ARCHIVE              += $(ARCHIVE_NAME).tar.gz
  _TAR_ARG		= czf
else
  ifeq ($(ARCHIVE_TYPE), bzip)
   _ARCHIVE             += $(ARCHIVE_NAME).tar.bz2
   _TAR_ARG		= cjf
  else
    $(warning unknown archive type $(ARCHIVE_TYPE), using bzip)
    _ARCHIVE            += $(ARCHIVE_NAME).tar.bz2
    _TAR_ARG		= cjf
  endif
endif

ifeq ($(LANGUAGE), CXX)
  CPPFLAGS		?= $(FLAGS)
  CXXFLAGS		?= $(LINK_FLAGS)
  _COMPILER		= $(CXX) $(CPPFLAGS)
  _LINKER		= $(CXX)
  _LINKER_FLAGS		= $(CXXFLAGS)
else
  ifeq ($(LANGUAGE), CC)
    CFLAGS		?= $(FLAGS)
    LDFLAGS		?= $(LINK_FLAGS)
    _COMPILER		= $(CC) $(CFLAGS)
    _LINKER		= $(CC) $(LDFLAGS)
    _LINKER_FLAGS	= $(LDFLAGS)
  else
    $(warning Unknown language $(LANGUAGE), using CXX)
    CPPFLAGS		?= $(FLAGS)
    CXXFLAGS		?= $(LINK_FLAGS)
    _COMPILER		= $(CXX) $(CPPFLAGS)
    _LINKER		= $(CXX)
    _LINKER_FLAGS	= $(CXXFLAGS)
  endif
endif

all: $(BIN)

run: all
	./$(BIN) $(ARGV)

$(BIN): $(_OBJS)
	$(_LINKER)	$(_OBJS)	$(_LINKER_FLAGS)	-o $@

$(OBJS_DIR)/%.c.o: $(SRC_DIR)/%.c
	@test -d `dirname $@` || mkdir -p `dirname $@`
	$(CC) -c $(_IFLAGS)  $(CFLAGS) -o $@     $<

$(OBJS_DIR)/%.cc.o: $(SRC_DIR)/%.cc
	@test -d `dirname $@` || mkdir -p `dirname $@`
	$(CXX) -c $(_IFLAGS) $(CPPFLAGS) -o $@     $<

$(OBJS_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	@test -d `dirname $@` || mkdir -p `dirname $@`
	$(CXX) -c $(_IFLAGS) $(CPPFLAGS) -o $@     $<

clean:
	test -z "$(_OBJS)" || $(RM) $(_OBJS)
	test -z "$(BIN)" || ! test -f "$(BIN)" || $(RM) $(BIN)
	test -z "$(OBJS_DIR)" || ! test -r "$(OBJS_DIR)" || $(RM) -r $(OBJS_DIR)
	test -z "$(CLEAN_FILES)" || $(RM) -r $(CLEAN_FILES)

distclean: clean
	test -z "$(DEP_FILE)" || ! test -f "$(DEP_FILE)" || $(RM) $(DEP_FILE)

dist: distclean
	$(RM) -r $(ARCHIVE_NAME)
	$(RM) $(_ARCHIVE)
	$(LOG) > ChangeLog || true
	mkdir -p $(ARCHIVE_NAME)
	cp -r $(DIST_FILES) $(ARCHIVE_NAME)
	$(TAR) $(_TAR_ARG) $(_ARCHIVE) $(ARCHIVE_NAME)
	$(RM) -r $(ARCHIVE_NAME)
	@echo "========================"
	$(TAR) tf $(_ARCHIVE)
	@echo "========================"
	$(CHECKSUM)  $(_ARCHIVE)
	$(RM) ChangeLog

-include $(DEP_FILE)

$(DEP_FILE): $(SOURCES)
	@$(RM) $(DEP_FILE);					\
	for f in $(SOURCES); do					\
	  obj=$(OBJS_DIR)/$${f#$(SRC_DIR)/};			\
	  $(_COMPILER) -MT$${obj}.o -MM $$f>> $(DEP_FILE);	\
	done;

#EOF
